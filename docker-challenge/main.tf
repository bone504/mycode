terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.22.0"
    }
  }
}

provider "docker" {}

resource "docker_image" "simplegoservice" {
  name         = "mhausenblas/simplegoservice:0.5.0"
  keep_locally = true       // keep image after "destroy"
}

resource "docker_container" "simplegoservice" {
  image = docker_image.simplegoservice.image_id
  name  = var.container_name
  ports {
    internal = var.internal_port
    external = var.external_port
  }
}

variable "container_name" {
  default = "AltaResearchWebService"
}

variable "internal_port" {
  description = "internal port to be used"
  type        = number
  default     = 9876
}

variable "external_port" {
  description = "external port to be used"
  type        = number
  default     = 5432
}


