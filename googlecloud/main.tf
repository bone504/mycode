terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.37.0"
    }
  }
}

provider "google" {
  # create by searching for "create project" in google
  project = "ugcslab-ssb-dev1"
  region  = "us-east4"
  zone    = "us-eas4-c"
}
